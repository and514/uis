# params_scenarios = {
#     "jsonrpc": "2.0",
#     "id": 9,
#     "method": "get.scenarios",
#     "params": {
#         "access_token": access_token,
#     }
# }

example_data = [
    {
     'id': 127778,
     'name': 'АСД Техн рабочий номер ',
     'sites': [],
     'virtual_phone_numbers': ['74999950533'],
     'campaigns': []
    },
    {
     'id': 121248,
     'name': 'не рабочий',
     'sites': [],
     'virtual_phone_numbers': [],
     'campaigns': []
    },
    {
     'id': 123468,
     'name': 'новакид-800-нерабочий',
     'sites': [],
     'virtual_phone_numbers': ['78002007716'],
     'campaigns': []
    },
    {
     'id': 123466,
     'name': 'новакид-800-рабочий',
     'sites': [],
     'virtual_phone_numbers': ['78002007716'],
     'campaigns': []
    },
    {
     'id': 59745,
     'name': 'Прямой-Азарову',
     'sites': [],
     'virtual_phone_numbers': [],
     'campaigns': []
    },
    {
     'id': 59744,
     'name': 'Рабочий',
     'sites': [],
     'virtual_phone_numbers': [],
     'campaigns': []
    },
    {
     'id': 114454,
     'name': 'с внутренним',
     'sites': [],
     'virtual_phone_numbers': [],
     'campaigns': []}
]