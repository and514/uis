#!/usr/bin/python
import requests
import psycopg2
import sys

# from example_data.call_legs_report import example_data as call_legs_report_example_data
# from example_data.employee import example_data as employee_example_data


uis_access_token = "1343ygtdmcgo7y1wjcyf7dn3rj9q9rui856j09en"
uis_url = 'https://dataapi.uiscom.ru/v2.0'
uis_date_from = '2018-09-08 00:00:00'
uis_date_till = '2018-09-18 00:00:00'


def create_table_calls(cur, table_calls_name, user_db):

    cur.execute(f'''
        CREATE TABLE IF NOT EXISTS {table_calls_name} (
          id serial NOT NULL,
          call_session_id integer,
          start_time timestamp without time zone NOT NULL,
          connect_time timestamp without time zone,
          employee_id integer,
          -- in, out
          direction VARCHAR(3),
          virtual_phone_number VARCHAR(20),
          called_phone_number VARCHAR(20),
          calling_phone_number VARCHAR(20),
          -- name text NOT NULL,
          duration integer,
          CONSTRAINT {table_calls_name}_pkey PRIMARY KEY (id)
          )
                ''')
    cur.execute(f'ALTER TABLE {table_calls_name} OWNER TO {user_db};')


def load_data_calls():
    params_call_legs_report = {
        "jsonrpc": "2.0",
        "id": 3,
        "method": "get.call_legs_report",
        "params": {
            "access_token": uis_access_token,
            "date_from": uis_date_from,
            "date_till": uis_date_till,
            # "offset": "number",
            # TODO проверять кол-во записей в ответе, если достигнут лимит, то грузить кусками
            "limit": 10000,
        }
    }

    resp = requests.post(uis_url, json=params_call_legs_report)
    r = resp.json()
    return r['result']['data']


def save_data_calls(cur, table_calls_name, rows):
    try:
        for i, row in enumerate(rows):
            if not row['is_operator']:
                continue
            command_str = f'''
                INSERT INTO {table_calls_name} 
                VALUES(
                    {row['id']},
                    {row['call_session_id']},
                    {row['start_time'] and "to_timestamp('%s', 'yyyy-mm-dd hh24:mi:ss')" % row['start_time'] or "NULL"}, 
                    {row['connect_time'] and "to_timestamp('%s', 'yyyy-mm-dd hh24:mi:ss')" % row['connect_time'] or "NULL"}, 
                    {row['employee_id'] or 'NULL'},
                    '{row['direction']}',
                    '{row['virtual_phone_number']}',
                    '{row['called_phone_number']}',
                    '{row['calling_phone_number']}',
                    {row['duration']}
                    )
                '''
            cur.execute(command_str)
    except Exception as e:
        print(f'Ошибка в {i} строке (save_data_calls)')
        raise e


def create_table_employee(cur, table_employee_name, user_db):

    cur.execute(f'''
        CREATE TABLE IF NOT EXISTS {table_employee_name} (
          id serial NOT NULL,
          first_name text NOT NULL,
          last_name text NOT NULL,
          full_name text NOT NULL,
          email text NOT NULL,
          phone_numbers text NOT NULL,
          CONSTRAINT {table_employee_name}_pkey PRIMARY KEY (id)
          )
                ''')
    cur.execute(f'ALTER TABLE {table_employee_name} OWNER TO {user_db};')


def load_data_employee():
    params_call_legs_report = {
        "jsonrpc": "2.0",
        "id": 3,
        "method": "get.employees",
        "params": {
            "access_token": uis_access_token,
        }
    }

    resp = requests.post(uis_url, json=params_call_legs_report)
    r = resp.json()
    return r['result']['data']


def save_data_employee(cur, table_employee_name, rows):
    try:
        for i, row in enumerate(rows):
            command_str = f'''
                INSERT INTO {table_employee_name} 
                VALUES(
                    {row['id']},
                    '{row['first_name']}',
                    '{row['last_name']}',
                    '{row['full_name']}',
                    '{row['email']}',
                    '{','.join([x['phone_number'] for x in row['phone_numbers']])}'
                    )
                '''
            cur.execute(command_str)
    except Exception as e:
        print(f'Ошибка в {i} строке (save_data_employee)')
        raise e


def run():
    # CREATE DATABASE uis OWNER novakid;
    con = None
    table_calls_name = 'calls'
    table_employees_name = 'employees'

    with open('conf.txt') as f:
        name_db, user_db, password_db = tuple([r.replace('\n', '') for r in f.readlines()])

    try:
        con = psycopg2.connect(f"host='localhost' dbname='{name_db}' user='{user_db}' password='{password_db}'")
        cur = con.cursor()

        create_table_calls(cur, table_calls_name, user_db)
        data = load_data_calls()
        # data = call_legs_report_example_data
        save_data_calls(cur, table_calls_name, data)

        # create_table_employee(cur, table_employees_name, user_db)
        # data = load_data_employee()
        # # data = employee_example_data
        # save_data_employee(cur, table_employees_name, data)

        con.commit()

    except psycopg2.DatabaseError as e:
        if con:
            con.rollback()
        print(str(e))
        sys.exit(1)

    finally:
        if con:
            con.close()


if __name__ == '__main__':
    run()