SELECT
  to_char(c.start_time::date, 'DD-MM-YYYY') AS "Date",
  -- c.employee_id,
  e.full_name AS "Name",
  COUNT(c.id) FILTER(where c.direction = 'in') AS "In calls count",
  SUM(c.duration) FILTER(where c.direction = 'in') AS "In calls duration",
  COUNT(c.id) FILTER(where c.direction = 'out') AS "Out calls count",
  SUM(c.duration) FILTER(where c.direction = 'out') AS "Out calls duration"
from calls AS c
  INNER JOIN employees AS e ON e.id = c.employee_id
GROUP BY c.start_time::date, c.employee_id, e.full_name
ORDER BY "Date", "Name"